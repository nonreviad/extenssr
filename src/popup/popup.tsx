import React, { useCallback, useState, useEffect } from 'react'
import Container from '@mui/material/Container'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormGroup from '@mui/material/FormGroup'
import Button from '@mui/material/Button'
import IconButton from '@mui/material/IconButton'
import ContentAndBackgroundMessageBroker from 'messaging/content_to_background_broker'
import Switch from '@mui/material/Switch'
import Slider from '@mui/material/Slider'
import { SettingsKeys } from 'storage/storage'
import { ChromeStorage } from 'storage/chrome_storage'
import CasinoIcon from '@mui/icons-material/Casino'

type TabPanelProps = {
  children: React.ReactNode,
  value: number,
  index: number
} & React.ComponentPropsWithoutRef<'div'>

function TabPanel(props: TabPanelProps): JSX.Element {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={'span'}>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

export type PopupProps = {
  messageBroker: ContentAndBackgroundMessageBroker
  storage: ChromeStorage
}

function useSetting<S extends keyof SettingsKeys>(storage: ChromeStorage, name: S): [SettingsKeys[S], (newValue: SettingsKeys[S]) => void] {
  const [value, setValue] = useState(storage.getCachedValue(name))
  useEffect(() => {
    const listener = storage.createListener(name, setValue)
    return () => listener.deregister()
  }, [storage, name])

  const update = useCallback((newValue: SettingsKeys[S]) => {
    // This should be redundant, but I don't wanna tempt fate.
    setValue(newValue)
    storage.setValue(name, newValue)
  }, [storage, name])

  return [value, update]
}
export type SettingToggleProp<Key extends keyof SettingsKeys> = {
  storageKey: Key,
  storage: ChromeStorage,
  label: string
}

function ToggleSetting<Key extends keyof SettingsKeys>({ storage, storageKey, label }: SettingToggleProp<Key>) {
  const [val, setVal] = useSetting(storage, storageKey)
  return <FormControlLabel
    label={label}
    control={
      <Switch
        color="primary"
        checked={val as boolean}
        onChange={(evt) => setVal(evt.target.checked as SettingsKeys[Key])}
      />
    }
  />
}

type TabProps = { storage: ChromeStorage, broker: ContentAndBackgroundMessageBroker, value: number, index: number }

function CameraEffectsTab({ storage, value, index, broker }: TabProps) {
  const [pixelate, setPixelate] = useSetting(storage, 'pixelateMap')
  const [pixelateScaling, setPixelateScaling] = useSetting(storage, 'pixelateScale')
  const [toon, setToon] = useSetting(storage, 'toon')
  const [toonScale, setToonScale] = useSetting(storage, 'toonScale')
  return <TabPanel value={value} index={index}>
    <FormGroup>
      <FormControlLabel
        label="Pixelate"
        control={
          <Switch
            color="primary"
            checked={pixelate}
            onChange={(evt) => {
              setPixelate(evt.target.checked)
            }}
          />
        }
      />
      {pixelate && <Slider
        value={pixelateScaling}
        min={4.0}
        max={300.0}
        onChange={(event, newValue) => setPixelateScaling(newValue as number)}
      />}
    </FormGroup>
    <FormGroup>
      <FormControlLabel
        label="Toon"
        control={
          <Switch
            color="primary"
            checked={toon}
            onChange={(evt) => {
              setToon(evt.target.checked)
            }}
          />
        }
      />
      {toon && <Slider
        value={toonScale}
        min={2.0}
        max={20.0}
        step={0.1}
        onChange={(_, newValue) => setToonScale(newValue as number)}
      />}
    </FormGroup>
    <ToggleSetting label={'Grayscale'} storageKey={'grayscale'} storage={storage} />
    <ToggleSetting label={'Invert colours'} storageKey={'invert'} storage={storage} />
    <ToggleSetting label={'Sepia effect'} storageKey={'sepia'} storage={storage} />
    <ToggleSetting label={'Mirror'} storageKey={'mirror'} storage={storage} />
    <ToggleSetting label={'Fish eye lens'} storageKey={'fisheye'} storage={storage} />
    <ToggleSetting label={'CRT TV filter'} storageKey={'chromaticAberration'} storage={storage} />
    <ToggleSetting label={'Edge filter'} storageKey={'sobel'} storage={storage} />
    <ToggleSetting label={'Drunk mode'} storageKey={'drunk'} storage={storage} />
    <ToggleSetting label={'Vignette'} storageKey={'vignette'} storage={storage} />
    <ToggleSetting label={'Water effect'} storageKey={'water'} storage={storage} />
    <ToggleSetting label={'Bloom'} storageKey={'bloom'} storage={storage} />
    <ToggleSetting label={'Min filter'} storageKey={'min'} storage={storage} />
    <ToggleSetting label={'Motion Blur'} storageKey={'motionBlur'} storage={storage} />
    <ToggleSetting label={'Screen scrambler'} storageKey={'scramble'} storage={storage} />
    <ToggleSetting label={'Hide Compass'} storageKey={'hideCompass'} storage={storage} />
    <ToggleSetting label={'Show Car'} storageKey={'showCar'} storage={storage} />
    <FormGroup>
      <IconButton onClick={ () => { broker.sendExternalMessage('randomize', null)}}>
        <CasinoIcon />
        I am feeling lucky
      </IconButton>
    </FormGroup>
  </TabPanel>
}

function SpecialEffectsTab({ storage, value, index }: TabProps) {
  return <TabPanel value={value} index={index}>
    <Container maxWidth="lg" style={{ maxHeight: 'lg', padding: 0 }}>
      <ToggleSetting label={'Make it snow on Streetview'} storageKey={'snowing'} storage={storage} />
      <ToggleSetting label={'Hide all cars (super slow and still a bit flaky!)'} storageKey={'aiOverlay'} storage={storage} />
      <div>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            window.open('https://www.geoguessr.com/#16bit')
          }}
        >
          Start text mode game
        </Button>
      </div>
    </Container>
  </TabPanel>
}
function OtherTab({ storage, value, index }: TabProps) {
  const [debuggable, setDebuggable] = useSetting(storage, 'debuggable')
  return <TabPanel value={value} index={index}>
    <ToggleSetting label={'Enable accesibility features'} storageKey={'enableAccessibilityMode'} storage={storage} />
    <ToggleSetting label={'Enable round timer'} storageKey={'enableTimer'} storage={storage} />
    <ToggleSetting label={'Round randomizer'} storageKey={'randomizer'} storage={storage} />
    <FormGroup>
      <FormControlLabel
        label="Enable debugging"
        control={
          <Switch
            color="primary"
            checked={debuggable}
            onChange={(evt) => setDebuggable(evt.target.checked)}
          />
        }
      />
    </FormGroup>
    <div>
      <Button
        variant="contained"
        color="primary"
        disabled={!debuggable}
        onClick={() => {
          storage.getValueCb('logs', logs => {
            storage.getValueCb('events', events => {
              const a = document.createElement('a')
              const items = { logs: logs, events: events }
              const file = new Blob([JSON.stringify(items)], { type: 'application/json' })
              a.href = URL.createObjectURL(file)
              a.download = 'bugreport.json'
              a.click()
              URL.revokeObjectURL(a.href)
            })
          })
        }}
      >
        Download full bug report
      </Button>
    </div>
    <div>
      <Button
        disabled={!debuggable}
        variant="contained"
        color="primary"
        onClick={() => {
          storage.setValue('events', [])
          storage.setValue('logs', [])
        }}
      >
        Clear debugging data
      </Button>
    </div>
    <div>
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          storage.clearAll().then(() => {
            window.location.reload()
          })
        }}
      >
        Clear storage
      </Button>
    </div>
  </TabPanel>
}

function Popup({ storage, messageBroker }: PopupProps): JSX.Element {
  const [selectedTab, setSelectedTab] = useState(0)
  let tabIdx = 0
  return (
    <Container maxWidth="lg" style={{ maxHeight: 'lg', padding: 0 }}>
      <AppBar color='primary' position="static">
        <Tabs textColor='inherit' indicatorColor='secondary' value={selectedTab} onChange={(_, value) => setSelectedTab(value)}>
          <Tab label="Camera effects" color='primary' />
          <Tab label="Special effects" color='primary' />
          <Tab label="Other" color='primary' />
        </Tabs>
      </AppBar>
      <CameraEffectsTab value={selectedTab} broker={messageBroker} index={tabIdx++} storage={storage} />
      <SpecialEffectsTab value={selectedTab} broker={messageBroker} index={tabIdx++} storage={storage} />
      <OtherTab value={selectedTab} broker={messageBroker} index={tabIdx++} storage={storage} />
    </Container>
  )
}
export default Popup
