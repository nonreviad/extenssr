# Extenssr

[![Extenssr](./icons/extenssr.png)](https://linktr.ee/extenssr)

A browser extension for extending the experience of [Geoguessr](https://www.geoguessr.com)

## Features

* Battle Royale extensions
    * [Show visited locations at the end of round](https://www.youtube.com/watch?v=Qu9uZupIc90)
* [Map filter effects](https://youtu.be/l74eCITtLhs)
    * Pixelate
    * Toon
    * Grayscale
    * Edge enhance
    * CRT TV like filter (chromatic aberration + gamma shift + periodic darkening)
    * Hide compass
    * Hide car
    * Winter mode ❄️
* [Country streaks in regular maps](https://www.youtube.com/watch?v=u-FVSPzHQPg)
    * This feature is **not** vetted for [Geotips world record attempts](https://geotips.net/world-records)
    * It currently only supports default settings (moving, panning, zooming allowed, no time limit)
* Saving locations found during regular rounds
* Co-op mode
* Cursed modes
    * Hide **all** cars
    * Text only mode

# Reporting bugs

The project is still very new, so bugs are expected. If you encounter a reproducible bug, please do the following:

* (optional - see last bullet point) obtain a bugreport json
    * enable debugging (from the debugging panel of the extension pop-up menu)
    * reproduce the issue
    * download full bug report (same debugging panel)
    * there is no process to anonymise the contents or strip unnecessary personal data; the data is only used for the purpose of fixing bugs, ands is not shared with other parties. If that still makes you uncomfortable, you can skip this step.
* send the bug report (if you're ok with the caveats) alongside a description of the issue to `extenssr at gmail dot com`.

## Building

Building requires additional secrets and assets that are not included in the repository.
There is a sample `.env` file in `.env.template`. That contains the `CLIENT_ID` for connecting to the Twitch bot, a manifest key for the Chrome extension (required for keeping a stable appid when developing locally) and a `GUID` required if developing a Firefox add-on.

To build, simply run
```
    npm install
    npm run chrome #for a chrome extension
    npm run firefox #for firefox
```
During the development cycle, you can use a webpack watch command
```
    npm run watch-chrome #for chrome, with hardcoded key
    npm run watch-chrome-nokey #for chrome, no hardcoded key
    npm run watch-firefox #for firefox
```
And load the `extenssr_chrome` or `extenssr_firefox` directory as an unpacked extension.
The Chrome extension published to the Web store is built with `npm run chrome-nokey` (the key is added automatically by the submission process).


# Acknowledgements

This is currently a project maintained by myself (`extenssr at gmail dot com`), but I stand on the shoulders of giants and rely on others to come up with good ideas.

Extenssr contributors:
* [ReAnna](https://gitlab.com/Annannanna)

Some of the folks below have donation links, just saying 😀.
* [@kyarosh_](https://www.instagram.com/kyarosh_) who designed the Extenssr icon.
* [Chatguessr](https://chatguessr.com/) folks, who conceived the original mod. This extension started off as an attempt to reimplement Chatguessr as an extension (to help out non-Windows streamers).
* [GeoGuessr Pro Community](https://discord.com/invite/xQQdRy5) which inspired a lot of the functionality. None of the scripts were copied, either due to licence incompatibility or because the extension uses different approaches.
    * ZackC__ who documented many of the Geoguessr API calls
    * [drparse](https://openuserjs.org/users/drparse) who created a bunch of map filter effects.
    * [SubSymmetry and theroei](https://www.reddit.com/r/geoguessr/comments/htgi42/country_streak_counter_script_automated_and/) who created the original country streak scripts.
    * Possibly others I forgot! Please send a pull request to add credit, or DM/email me.
* The Geoguessr community over on Twitch. Some folks who have helped either by suggesting features, or by alpha testing some of the functionality.
    * [El Porco](https://www.twitch.tv/el_porco_)
    * [Fran](https://twitch.tv/froonb)
    * [Jasmine](https://twitch.tv/jasminelune)
    * [Nhoa](https://twitch.tv/nhoska)
    * [ReAnna](https://twitch.tv/reanna__)
    * Probably many others I've forgotten, I'm sorry :(. Support your friendly local [Geoguessr streamers](https://www.twitch.tv/directory/game/GeoGuessr)!

# License

This extension is distributed under [Apache 2.0 license](./LICENSE), with the exception of:

* [Shrug by Kimi Lewis from the Noun Project](./icons/shrug.svg) which is licensed under Creative Commons. Taken from [The Noun Project](https://thenounproject.com/term/shrug/622363/)
* [Snow shader](https://www.shadertoy.com/view/ldsGDn) is licensed under [Creative Commons Attribution-NonCommercial-ShareAlike](http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US). Original shader by Andrew Baldwin.
* [Flying Santa Animation](https://codepen.io/Coding-Artist/pen/ExaXZqZ) has no specified license, but giving it a hat tip here.
* Bundled countries GeoJson data is from [datahub.io](https://datahub.io/core/geo-countries), originally from [Natural Earth](http://www.naturalearthdata.com/) is licensed under the [Open Data Commons Public Domain Dedication and License](https://opendatacommons.org/licenses/pddl/1-0/). The original data is public domain.